import eslintPlugin from "vite-plugin-eslint";

const viteAutoFix: boolean = process.env.VITE_AUTO_FIX === "true";

// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
	srcDir: "src",
	telemetry: true,

	typescript: {
		strict: true,
	},

	modules: [
		// https://github.com/kevinmarrec/nuxt-pwa-module
		"@kevinmarrec/nuxt-pwa",
	],

	pwa: {
		icon: false,
	},

	vite: {
		// https://vitejs.dev/config/server-options.html
		server: {
			strictPort: true,
		},

		plugins: [
			eslintPlugin({
				fix: viteAutoFix,
			}),
		],
	},
});
