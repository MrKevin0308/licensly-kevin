import {
	getAssetFromKV,
	NotFoundError,
	MethodNotAllowedError,
} from "@cloudflare/kv-asset-handler";

import manifestJSON from "__STATIC_CONTENT_MANIFEST";
import { Bindings } from "./env";

const assetManifest = JSON.parse(manifestJSON);

export async function handleStatic(
	request: Request,
	bindings: Bindings,
	context: ExecutionContext
) {
	try {
		return await getAssetFromKV(
			{
				request,
				waitUntil(promise: Promise<any>) {
					return context.waitUntil(promise);
				},
			},
			{
				ASSET_NAMESPACE: bindings.__STATIC_CONTENT,
				ASSET_MANIFEST: assetManifest,
			}
		);
	} catch (e) {
		if (e instanceof NotFoundError) {
			return new Response(e.message, {
				status: e.status,
			});
		} else if (e instanceof MethodNotAllowedError) {
			return new Response(e.message, {
				status: e.status,
			});
		} else {
			console.log(e);

			return new Response("An unexpected error occurred", {
				status: 500,
			});
		}
	}
}
