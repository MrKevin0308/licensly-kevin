import type { Bindings } from "./env";
import { handleStatic } from "./static";
import { handleTussle } from "./tussle";

const tusslePrefix = "/files";

async function handleRequest(
	request: Request,
	bindings: Bindings,
	context: ExecutionContext
) {
	const { pathname } = new URL(request.url);

	if (pathname === tusslePrefix || pathname.startsWith(`${tusslePrefix}/`)) {
		return await handleTussle(request, bindings, context);
	}

	return await handleStatic(request, bindings, context);
}

const worker: ExportedHandler<Bindings> = {
	fetch: handleRequest,
};

export default worker;
